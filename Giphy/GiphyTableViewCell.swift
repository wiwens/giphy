//
//  asdasd.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 24.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

class GiphyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var giphyImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        giphyImageView.image = nil
    }
    
}
