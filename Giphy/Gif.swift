//
//  Gif.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import Unbox

struct Gif: Unboxable {
    
    let imageUrl: String
    let height: CGFloat
    let width: CGFloat
    
    init(unboxer: Unboxer) {
        imageUrl = unboxer.unbox("images.fixed_width.url", isKeyPath: true)
        height = unboxer.unbox("images.fixed_width.height", isKeyPath: true)
        width = unboxer.unbox("images.fixed_width.width", isKeyPath: true)
    }
    
}
