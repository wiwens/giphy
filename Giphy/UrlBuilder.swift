//
//  UrlBuilder.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation

class UrlBuilder {
    
    func buildURLWithPath(path: String) -> NSURL {
        let urlComponents = NSURLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = "api.giphy.com"
        urlComponents.path = path
        return urlComponents.URL!
    }
    
}
