//
//  GiphyService.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Alamofire
import Unbox

let APIkey = "dc6zaTOxFJmzC"

protocol GiphyServiceProtocol {
    func fetchGifsWithSearchQuery(searchQuery: String, offset: String, completion: (gifs: [Gif]?, pagination: Pagination?, error: NSError?) -> ())
}

class GiphyService: GiphyServiceProtocol {
    
    func fetchGifsWithSearchQuery(searchQuery: String, offset: String, completion: (gifs: [Gif]?, pagination: Pagination?, error: NSError?) -> ()) {
        let path = "/v1/gifs/search"
        Alamofire.request(.GET, UrlBuilder().buildURLWithPath(path), parameters: ["q":searchQuery, "api_key":APIkey, "offset":offset]).responseData { (response: Response<NSData, NSError>) in
            switch response.result {
            case .Success(_):
                guard let data = response.data else { return }
                let result = Mapper().mapData(data)
                let gifs: [Gif]? = result.0
                let pagination: Pagination? = result.1
                completion(gifs: gifs, pagination: pagination ,error: nil)
            case .Failure(let error):
                completion(gifs: nil, pagination: nil, error: error)
            }
        }
    }
    
}
