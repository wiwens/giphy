//
//  ViewController.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 24.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SwiftSpinner

class GiphyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let giphyService = GiphyService()
    private var gifsArray = [Gif]()
    private var pagination: Pagination?
    private var offset = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerNibs()
        setupAppearance()
        configureBackgroundIfNeeded()
        initializeInfinityScroll()
    }
    
    private func registerNibs() {
        tableView.registerNib(UINib(nibName: String(GiphyTableViewCell), bundle: nil), forCellReuseIdentifier: String(GiphyTableViewCell))
    }
    
    private func computeRatio(width: CGFloat) -> CGFloat {
        return view.frame.width / width
    }
    
    private func fetchGifsWithSearchQuery(searchQuery: String, offset: String) {
        SwiftSpinner.show("Fetching gifs...")
        giphyService.fetchGifsWithSearchQuery(searchQuery, offset: offset) { [weak self] (gifs, pagination, error) in
            SwiftSpinner.hide()
            if let error = error {
                print("Error \(error)")
            }
            if let gifs = gifs {
                self?.gifsArray.appendContentsOf(gifs)
                self?.configureBackgroundIfNeeded()
                self?.pagination = pagination
                self?.offset += pagination?.count ?? 0
                self?.tableView.reloadData()
                self?.tableView.finishInfiniteScroll()
            }
        }
    }

    private func initializeInfinityScroll() {
        tableView.infiniteScrollIndicatorStyle = .White
        tableView.addInfiniteScrollWithHandler { [weak self] (scrollView) -> Void in
            self?.tableView = scrollView as? UITableView
            SDImageCache.sharedImageCache().clearMemory()
            SDImageCache.sharedImageCache().cleanDisk()
            guard let `self` = self else { return }
            self.fetchGifsWithSearchQuery(self.searchBar.text!, offset: String(self.offset))
        }
    }
    
    private func configureBackgroundIfNeeded() {
        if gifsArray.isEmpty {
            let giphyPlaceholder = NSBundle.mainBundle().loadNibNamed("GiphyNotFoundPlaceholder", owner: self, options: nil)[0] as! UIView
            tableView.backgroundView = giphyPlaceholder
        } else {
            tableView.backgroundView = nil
        }
    }
    
    // MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if let search = searchBar.text {
            gifsArray.removeAll()
            offset = 0
            fetchGifsWithSearchQuery(search, offset: String(offset))
            searchBar.resignFirstResponder()
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return gifsArray[indexPath.row].height * computeRatio(gifsArray[indexPath.row].width)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gifsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let giphyCell = tableView.dequeueReusableCellWithIdentifier(String(GiphyTableViewCell), forIndexPath: indexPath) as! GiphyTableViewCell
        giphyCell.giphyImageView.sd_setImageWithURL(NSURL(string: gifsArray[indexPath.row].imageUrl), placeholderImage: UIImage(named: "placeholder"))
        return giphyCell
    }
    
}
