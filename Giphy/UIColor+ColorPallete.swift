//
//  UIColor+ColorPallete.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func searchBarBackgroundColor() -> UIColor {
        return UIColor(red:0.10, green:0.14, blue:0.18, alpha:1.0)
    }
    
    class func searchBarBorderColor() -> UIColor {
        return UIColor(red:0.10, green:0.14, blue:0.18, alpha:1.0)
    }

    class func searchFieldBackgroudColor() -> UIColor {
        return UIColor(red:0.10, green:0.14, blue:0.18, alpha:1.0)
    }
    
    class func searchFieldTextColor() -> UIColor {
        return UIColor.whiteColor()
    }
    
    class func searchFieldBorderColor() -> UIColor {
        return UIColor(red:0.00, green:0.92, blue:1.00, alpha:1.0)
    }
    
    class func applicationBackgroundColor() -> UIColor {
        return UIColor(red:0.10, green:0.14, blue:0.18, alpha:1.0)
    }
    
    class func tableViewBackgroundColor() -> UIColor {
        return UIColor(red:0.10, green:0.14, blue:0.18, alpha:1.0)
    }
    
}
