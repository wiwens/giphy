//
//  Pagination.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit
import Unbox

struct Pagination: Unboxable {
    
    let totalCount: Int
    let count: Int
    let offset: Int
    
    init(unboxer: Unboxer) {
        totalCount = unboxer.unbox("total_count")
        count = unboxer.unbox("count")
        offset = unboxer.unbox("offset")
    }
    
}
