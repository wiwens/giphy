//
//  Mapper.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Unbox

class Mapper {
    
    func mapData(data: NSData) -> ([Gif]?, Pagination?) {
        do {
            let result: Results = try Unbox(data)
            return (result.results, result.pagination)
        } catch {
            print("Error with mapping data")
        }
        return (nil, nil)
    }
    
}