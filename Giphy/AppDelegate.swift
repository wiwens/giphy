//
//  AppDelegate.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 24.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        configureAppearance()
        return true
    }
    
    func configureAppearance() {
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }

}
