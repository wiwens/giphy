//
//  Results.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 25.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import Unbox

struct Results: Unboxable {
    
    let results: [Gif]
    let pagination: Pagination
    
    init(unboxer: Unboxer) {
        results = unboxer.unbox("data")
        pagination = unboxer.unbox("pagination")
    }
    
}