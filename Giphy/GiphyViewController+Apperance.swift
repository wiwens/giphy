//
//  GiphyViewController+Apperance.swift
//  Giphy
//
//  Created by Wiktor Wielgus on 26.05.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation

extension GiphyViewController {
    
    // MARK: Appearance
    
    func setupAppearance() {
        view.backgroundColor = UIColor.applicationBackgroundColor()
        configureTableView()
        configureSearchBar()
    }
    
    private func configureTableView() {
        tableView.separatorStyle = .None
        tableView.backgroundColor = UIColor.tableViewBackgroundColor()
    }
    
    private func configureSearchBar() {
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.searchBarBorderColor().CGColor
        searchBar.setImage(UIImage(named: "ic_search_blue"), forSearchBarIcon: .Search, state: .Normal)
        
        let searchField = searchBar.valueForKey("_searchField") as? UITextField
        
        if let searchField = searchField {
            searchField.backgroundColor = UIColor.searchFieldBackgroudColor()
            searchField.layer.borderWidth = 1.0
            searchField.layer.cornerRadius = 20.0
            searchField.layer.borderColor = UIColor.searchFieldBorderColor().CGColor
            searchField.font = UIFont(name: "Lato-Light", size: 15)
            searchField.textColor = UIColor.searchFieldTextColor()
            //after viewDidLayoutSubviews searchbar does something with textfield frame
            //so we are jumping out of stack to be executed after when these changes occur.
            dispatch_async(dispatch_get_main_queue(), {
                searchField.frame = CGRectMake(22, 10, self.view.frame.width - 44 , 40)
            })
        }
    }
    
}